//Gilberto E. Cancel Morales, Juan J. Corchado
//CCOM3033-001

#include <QCoreApplication>
#include <QDebug>
#include "filemanip.h"
#include <iostream>
#include <string>
#include "movie.h"

using namespace std ;


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    filemanip file("/home/eip/labs/functions-dvdinfo/dvd_csv.txt") ;
    string rating, year, genre, name, studio ;
    string movieinfo ;

    // YOUR CODE GOES HERE
    // TU CODIGO VA AQUI


    //Displays the movies in the file from 80 to 100.
    //Demuestra peliculas de la posicion 80 hasta la 100.
    //showMovies(file,80,100);


    //Displays movies with forrest gump in the title.
    //Demuestra peliculas con forrest gump en el titulo.
    //showMovies(file,"forrest gump");


    //Displays all information of movie in posicion 75125.
    //Demuestra la pelicula en la posicion 75125.
    //showMovie(getMovieByPosition(75125,file));


    /*
Displays the name and rating of the movie in the posicion 75125.
Demuestra el nombre y el rating the la pelicula en la posicion 75125.
    name = getMovieRating(getMovieByPosition(75125,file));
    rating = getMovieName(getMovieByPosition(75125,file));

    cout << name << endl;
    cout << rating << endl;
    */


    /*

Displays the informacion of the movie in line 75125 on a single line.
Demuestra la informacion de la pelicula de 75125 en una linea.
    movieinfo = getMovieByPosition(75125,file);
    name = getMovieName(movieinfo);
    rating = getMovieRating(movieinfo);
    year =  getMovieYear(movieinfo) ;
    genre = getMovieGenre(movieinfo) ;

    getMovieInfo(movieinfo, name, rating, year, genre);
    */


    /*
Displays the movie studio of the movie in line 75125.
Demuestra el movie studio de la pelicula de 75125.
    movieinfo = getMovieByPosition(75125,file);
    studio = getMovieStudio(movieinfo);

    cout << studio << endl;
    */


    /*
Overloaded function that includes the movie studio of the movie in line 75125.
Funcion sobrecargada con studio de la pelicula 75125.
    movieinfo = getMovieByPosition(75125,file);
    name = getMovieName(movieinfo);
    studio = getMovieStudio(movieinfo);
    rating = getMovieRating(movieinfo);
    year =  getMovieYear(movieinfo);
    genre = getMovieGenre(movieinfo);

    getMovieInfo(movieinfo,name,studio,rating,year,genre);
    */


    //Shows the function showMovie in a single line.
    //Funcion showMovie en una linea.
    //showMovieInLine(getMovieByPosition(75125,file));


    //Function showMovie that executes between lines 80 to 100.
    //Funcion showMovie en una linea entre un rango de posiciones.
    //showMovieInLine(file, 80, 100);

    cout << "\n\nTHE END" <<endl;
    return a.exec();
}
