#include "filemanip.h"

/// \fn filemanip::filemanip()
/// \~English
/// \brief Constructor that sets a fixed dvd file
/// \~Spanish
/// \brief Constructor para establece un archivo de dvd fijo
filemanip::filemanip()
{
    filemanip(":/dvd_csv.txt") ;
}

/// \fn filemanip::filemanip(QString filename)
/// \~English
/// \brief Constructor that sets a file
/// \~Spanish
/// \brief Constructor para establece un archivo de dvd
filemanip::filemanip(QString filename){
    file = new QFile(filename);
    file->open(QIODevice::ReadOnly) ;
    if(file->exists())
        in = new QTextStream(file) ;
    else {
        qDebug() << "Cant open " + filename + "! Exiting program..." ;
        exit(1);
    }

}

/// \fn QString filemanip::getnext(){
/// \~English
/// \brief Returns the next file text line
/// \~Spanish
/// \brief Devuelve la proxima linea de texto en el archivo
QString filemanip::getnext(){
    if(!in->atEnd())
           return in->readLine() ;
    return "" ;
}

filemanip::~filemanip(){
    delete in ;
    file->close() ;
    delete file ;
}

/// \fn void filemanip::reset()
/// \~English
/// \brief File pointer points to the start of the file
/// \~Spanish
/// \brief Apuntador del archivo apunta al inicio del archivo.
void filemanip::reset(){
    in->seek(0) ;
}
