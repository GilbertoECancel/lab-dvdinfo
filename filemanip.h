#ifndef FILEMANIP_H
#define FILEMANIP_H
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QDebug>

/// \~English
/// A class to manipulate files.
/// \~Spanish
/// Una clase para manipular archivos.
class filemanip
{
public:
    /// \fn filemanip::filemanip()
    /// \~English
    /// \brief Constructor that sets a fixed dvd file
    /// \~Spanish
    /// \brief Constructor para establece un archivo de dvd fijo
    filemanip() ;

    /// \fn filemanip::filemanip(QString filename)
    /// \~English
    /// \brief Constructor that sets a file
    /// \~Spanish
    /// \brief Constructor para establece un archivo de dvd
    filemanip(QString filename);

    /// \fn QString filemanip::getnext(){
    /// \~English
    /// \brief Returns the next file text line
    /// \~Spanish
    /// \brief Devuelve la proxima linea de texto en el archivo
    QString getnext() ;

    /// \fn void filemanip::reset()
    /// \~English
    /// \brief File pointer points to the start of the file
    /// \~Spanish
    /// \brief Apuntador del archivo apunta al inicio del archivo.
    void reset() ;

    /// \fn filemanip::~filemanip()
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~filemanip() ;
private:
    QFile *file ; /**< pointer to a file / apuntador a un archivo */
    QTextStream *in ; /**< pointer to a text stream / apuntador a un flujo de entrada */
};

#endif // FILEMANIP_H
