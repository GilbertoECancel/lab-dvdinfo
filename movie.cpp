#include "movie.h"

string getMovieByPosition(int position, filemanip &file){
    int counter = 0 ;

    //Input Validation
    if (position < 1){
        return "" ;
    }

    file.reset();

    QString line = file.getnext() ;
    while(line != ""){
        if (counter == position){
            return line.toStdString() ;
        }
        line = file.getnext() ;
        counter++ ;
    }
    return "" ;

}


string getMovieByName(string moviename, filemanip &file){
    QString movie = QString::fromStdString(moviename) ;
    QStringList fields ;

    file.reset();

    QString line = file.getnext() ;
    while(line != "") {
        fields = line.split("|");
        if (fields[0] == movie){
            return line.toStdString() ;
        }
        line = file.getnext() ;
    }
    return "" ;
}

void showMovie(string movieinfo){
    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if(fields.size() >= 15){
        cout << "==================== MOVIE =======================" <<endl
             << "Name:\t" << fields[0].toStdString() <<endl
             << "Rating:\t" << fields[7].toStdString() << endl
             << "Year:\t" << fields[8].toStdString() <<endl
             << "Genre:\t" << fields[9].toStdString() <<endl ;
    }

}

void showMovies(filemanip &file, int start, int end){
    int counter = 0;
    if(start < 1 || end < start)
        return ;

    file.reset();

    QString line = file.getnext() ;
    while(line != "") {
        if(counter >= start && counter <=end){
            showMovie(line.toStdString()) ;
        }
        line = file.getnext() ;
        counter++ ;
    }
}

void showMovies(filemanip &file, string keyword){

    QStringList fields ;

    if(keyword.size() < 1)
        return ;

    file.reset();

    QString line = file.getnext() ;
    while(line != "") {
        fields = line.split('|') ;
        if(fields[0].contains(QString::fromStdString(keyword),  Qt::CaseInsensitive)){
            showMovie(line.toStdString()) ;
        }
        line = file.getnext() ;
    }

}

string getMovieName(string movieinfo){

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15)
        return fields[0].toStdString() ;

    return "" ;

}
string getMovieRating(string movieinfo){

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15)
        return fields[7].toStdString() ;

    return "" ;

}

string getMovieYear(string movieinfo){

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15)
        return fields[8].toStdString() ;

    return "" ;

}

string getMovieGenre(string movieinfo){

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15)
        return fields[9].toStdString() ;

    return "" ;

}

void getMovieInfo(string movieinfo, string &nombre,  string &rating, string &year, string &genre){

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15){
        nombre = fields[0].toStdString() ;
        rating = fields[7].toStdString() ;
        year   = fields[8].toStdString() ;
        genre  = fields[9].toStdString() ;

        cout << nombre << rating << year << genre << endl;

    }

    return ;

}

//Function to obtain the studio how made the movie.
//Funcion para obtener el studio.
string getMovieStudio(string movieinfo)
    {

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15)
        return fields[1].toStdString() ;

    return "" ;

    }

//Overloaded function.
//Funcion sobrecargada.
void getMovieInfo(string movieinfo, string &nombre, string &studio , string &rating, string &year, string &genre)
{

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15){
        nombre = fields[0].toStdString() ;
        studio = fields[1].toStdString() ;
        rating = fields[7].toStdString() ;
        year   = fields[8].toStdString() ;
        genre  = fields[9].toStdString() ;

        cout << nombre << " " << studio << " " << rating << endl
             << " " << year << " " << genre << endl;

    }

    return ;
}

//Function that displays de informacion of movies in a single line.
//Funcion que demuestra la informacion de showMovies en una sola linea.
void showMovieInLine(string movieinfo){
    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if(fields.size() >= 15){
        cout
             << fields[0].toStdString() << " "
             << fields[7].toStdString() << " "
             << fields[8].toStdString() << " "
             << fields[9].toStdString() <<endl ;
    }

}

//Function that displays the informacion of movies in a single line between a range of posicions.
//Funcion que demuestra la informacion de showMovies en una sola linea, entre un rango de posiciones.
void showMovieInLine(filemanip file, int start, int end){

    while(start < end)
     {

    string movieinfo = getMovieByPosition(start, file);

    QStringList fields = QString::fromStdString(movieinfo).split('|') ;

    if (fields.size() >= 15)
        {
        string nombre = fields[0].toStdString() ;
        string rating = fields[7].toStdString() ;
        string year   = fields[8].toStdString() ;
        string genre  = fields[9].toStdString() ;

        cout << nombre << " " << rating << " " << year << " " << genre << endl;

        start++;
    }


        }

}

